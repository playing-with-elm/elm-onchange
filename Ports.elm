port module Ports exposing (..)


type alias AudioPortData =
    { content : String
    , filename : String
    }


port fileSelected : String -> Cmd msg


port fileContentRead : (AudioPortData -> msg) -> Sub msg
