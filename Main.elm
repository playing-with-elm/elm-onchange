module Main exposing (..)

import Html exposing (Html, text, program, audio, input, div)
import Html.Attributes exposing (src, title, class, id, type_, controls)
import Html.Events exposing (on)
import Json.Decode as Json
import Ports exposing (AudioPortData, fileSelected, fileContentRead)


type Msg
    = OnFileSelected
    | AudioRead AudioPortData


type alias Audio =
    { content : String
    , filename : String
    }


type alias Model =
    { id : String
    , mAudio : Maybe Audio
    }


main : Program Never Model Msg
main =
    program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( { id = "AudioInputId"
      , mAudio = Nothing
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnFileSelected ->
            ( model
            , fileSelected model.id
            )

        AudioRead data ->
            let
                newAudio =
                    { content = data.content
                    , filename = data.filename
                    }
            in
                ( { model | mAudio = Just newAudio }
                , Cmd.none
                )


view : Model -> Html Msg
view model =
    let
        audioPlayer =
            case model.mAudio of
                Just i ->
                    viewAudioPlayer i

                Nothing ->
                    text ""
    in
        div []
            [ input
                [ type_ "file"
                , id model.id
                , on "change"
                    (Json.succeed OnFileSelected)
                ]
                []
            , audioPlayer
            ]


viewAudioPlayer : Audio -> Html Msg
viewAudioPlayer a =
    audio
        [ id "audio"
        , type_ "audio/mp3"
        , src a.content
        , title a.filename
        , controls True
        ]
        []


subscriptions : Model -> Sub Msg
subscriptions model =
    fileContentRead AudioRead
