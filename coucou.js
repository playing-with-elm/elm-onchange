var app = Elm.Main.fullscreen();

app.ports.fileSelected.subscribe(function(id) {

    var node = document.getElementById(id);

    if (node === null) {
	return;
    }
    
    // If your file upload field allows multiple files, you might
    // want to consider turning this into a `for` loop.
    var file = node.files[0];
    
    var reader = new FileReader();

    reader.onload = (function(event) {

        var base64encoded = event.target.result;

        var portData = {
            content: base64encoded,
            filename: file.name
        };

        app.ports.fileContentRead.send(portData);
    });

    reader.readAsDataURL(file);
});
